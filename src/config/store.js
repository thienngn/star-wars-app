import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import reducers from '../reducers/index';

export const store = compose(
  applyMiddleware(thunk),
  GLOBAL.reduxNativeDevTools ? GLOBAL.reduxNativeDevTools(/* options */) : nope => nope,
)(createStore)(reducers);
