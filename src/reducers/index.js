import { combineReducers } from 'redux';
import categories from './categories';
import search from './search';

const rootReducer = combineReducers({
  categories,
  search,
});

export default rootReducer;
