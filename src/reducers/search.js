import { SEARCH_QUERY_CHANGED, CLEAR_SEARCH_QUERY } from '../actions/search';

const initialState = {
  searchQuery: '',
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SEARCH_QUERY_CHANGED:
      return { ...state, searchQuery: action.text };
    case CLEAR_SEARCH_QUERY:
      return { ...state, searchQuery: '' };
    default:
      return state;
  }
};

export default reducer;
