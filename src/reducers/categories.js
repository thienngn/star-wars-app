import {
  FETCHING_CATEGORIES_BEGIN,
  FETCHING_CATEGORIES_SUCCESS,
  FETCHING_CATEGORIES_FAILURE,
  FETCHING_CATEGORY_ITEMS_BEGIN,
  FETCHING_CATEGORY_ITEMS_SUCCESS,
  FETCHING_CATEGORY_ITEMS_FAILURE,
} from '../actions/categories';

const initialState = {
  categories: {},
  selectedCategory: null,
  items: {},
  isFetching: false,
  error: null,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case FETCHING_CATEGORIES_BEGIN:
      return {
        ...state,
        isFetching: true,
      };
    case FETCHING_CATEGORIES_SUCCESS:
      return {
        ...state,
        isFetching: false,
        categories: action.data,
      };
    case FETCHING_CATEGORIES_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: action.err,
      };
    case FETCHING_CATEGORY_ITEMS_BEGIN:
      return {
        ...state,
        isFetching: true,
      };
    case FETCHING_CATEGORY_ITEMS_SUCCESS:
      return {
        ...state,
        isFetching: false,
        selectedCategory: action.category,
        items: {
          [action.category]: action.data,
        },
      };
    case FETCHING_CATEGORY_ITEMS_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: action.err,
      };
    default:
      return state;
  }
};
