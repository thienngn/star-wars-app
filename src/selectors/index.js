import { createSelector } from 'reselect';

const selectedCategorySelector = state => state.categories.selectedCategory;
const itemsSelector = state => state.categories.items;
const inputQuerySelector = state => state.search.searchQuery;

const getFilteredItems = (selectedCategory, items, query) => {
  const itemsInCategory = items[selectedCategory];
  if (selectedCategory === 'films') {
    return itemsInCategory
      ? itemsInCategory.results.filter(item => item.title.indexOf(query) > -1)
      : [];
  }
  return itemsInCategory
    ? itemsInCategory.results.filter(item => item.name.indexOf(query) > -1)
    : [];
};

const getFilteredItemsSelector = createSelector(
  selectedCategorySelector,
  itemsSelector,
  inputQuerySelector,
  getFilteredItems,
);

export default getFilteredItemsSelector;
