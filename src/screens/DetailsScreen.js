import React from 'react';
import {
  ActivityIndicator, FlatList, StyleSheet, Text, View, TextInput,
} from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { fetchItemsForCategory } from '../actions/categories';
import { searchQueryChanged } from '../actions/search';
import getFilteredItemsSelector from '../selectors';
import Colors from '../styles/Colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.grey,
    paddingTop: 50,
  },
  item: {
    fontSize: 12,
    textAlign: 'center',
    paddingVertical: 10,
  },
  textInput: {
    borderColor: Colors.sky,
    borderWidth: 2,
    height: 45,
    width: 100,
    padding: 10,
  },
});

class DetailsScreen extends React.Component {
  static navigationOptions = {
    title: 'Detail',
  };

  renderListItem = ({ item }) => (
    <View key={item}>
      <Text style={styles.item}>
        {item.title || item.name}
      </Text>
    </View>
  );

  onChangeSearchQuery = (text) => {
    this.props.searchQueryChanged(text);
  };

  keyExtractor = item => item.id;

  render() {
    const {
      isFetching, selectedCategory, items, searchQuery,
    } = this.props;

    return (
      <View style={styles.container}>
        <Text>
          Category:
          {selectedCategory}
        </Text>
        <TextInput
          style={styles.textInput}
          onChangeText={text => this.onChangeSearchQuery(text)}
          value={searchQuery}
          placeholder="Search item"
        />
        {isFetching ? (
          <View>
            <ActivityIndicator size="large" />
            <Text>Fetching some data..</Text>
          </View>
        ) : (
          <View>
            <FlatList
              data={items}
              keyExtractor={this.keyExtractor}
              renderItem={this.renderListItem}
            />
          </View>
        )}
      </View>
    );
  }
}

DetailsScreen.defaultProps = {
  selectedCategory: null,
};

DetailsScreen.propTypes = {
  isFetching: PropTypes.bool.isRequired,
  selectedCategory: PropTypes.string,
  items: PropTypes.instanceOf(Object).isRequired,
  searchQuery: PropTypes.string.isRequired,
  searchQueryChanged: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  selectedCategory: state.categories.selectedCategory,
  items: getFilteredItemsSelector(state),
  isFetching: state.categories.isFetching,
  searchQuery: state.search.searchQuery,
});

const mapDispatchToProps = dispatch => ({
  ...bindActionCreators({ fetchItemsForCategory, searchQueryChanged }, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(DetailsScreen);
