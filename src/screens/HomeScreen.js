import React from 'react';
import {
  StyleSheet, Text, View, TouchableOpacity, ActivityIndicator, FlatList,
} from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { fetchCategories, fetchItemsForCategory } from '../actions/categories';
import Colors from '../styles/Colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.grey,
    paddingTop: 50,
  },
  item: {
    paddingVertical: 15,
  },
});

class HomeScreen extends React.Component {
  static navigationOptions = {
    title: 'Star Wars',
  };

  componentWillMount() {
    this.props.fetchCategories();
  }

  onPressListItem = (category) => {
    this.props.fetchItemsForCategory(category);
    this.props.navigation.navigate('Details');
  };

  renderListItem = ({ item }) => (
    <TouchableOpacity key={item} onPress={() => this.onPressListItem(item)}>
      <Text style={styles.item}>{item}</Text>
    </TouchableOpacity>
  );

  keyExtractor = item => item.toString();

  render() {
    const { isFetching, categories } = this.props;
    const categoryList = Object.keys(categories);

    return (
      <View style={styles.container}>
        {isFetching && (
          <View>
            <ActivityIndicator size="large" />
            <Text>Booting up the dark side..</Text>
          </View>
        )}
        <FlatList
          data={categoryList}
          keyExtractor={this.keyExtractor}
          renderItem={this.renderListItem}
        />
      </View>
    );
  }
}

HomeScreen.propTypes = {
  navigation: PropTypes.instanceOf(Object).isRequired,
  isFetching: PropTypes.bool.isRequired,
  fetchCategories: PropTypes.func.isRequired,
  fetchItemsForCategory: PropTypes.func.isRequired,
  categories: PropTypes.instanceOf(Object).isRequired,
};

const mapStateToProps = state => ({
  categories: state.categories.categories,
  isFetching: state.categories.isFetching,
});

const mapDispatchToProps = dispatch => ({
  ...bindActionCreators({ fetchCategories, fetchItemsForCategory }, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);
