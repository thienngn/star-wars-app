export const BASE_URL = 'https://swapi.co/api/';

// Types
export const FETCHING_CATEGORIES_BEGIN = 'FETCHING_CATEGORIES_BEGIN';
export const FETCHING_CATEGORIES_SUCCESS = 'FETCHING_CATEGORIES_SUCCESS';
export const FETCHING_CATEGORIES_FAILURE = 'FETCHING_CATEGORIES_FAILURE';
export const FETCHING_CATEGORY_ITEMS_BEGIN = 'FETCHING_CATEGORY_ITEMS_BEGIN';
export const FETCHING_CATEGORY_ITEMS_SUCCESS = 'FETCHING_CATEGORY_ITEMS_SUCCESS';
export const FETCHING_CATEGORY_ITEMS_FAILURE = 'FETCHING_CATEGORY_ITEMS_FAILURE';

// Action Creators
export const getCategoriesBegin = () => ({
  type: FETCHING_CATEGORIES_BEGIN,
});

export const getCategoriesSuccess = data => ({
  type: FETCHING_CATEGORIES_SUCCESS,
  data,
});

export const getCategoriesFailure = err => ({
  type: FETCHING_CATEGORIES_FAILURE,
  err,
});

export const fetchCategories = () => (dispatch) => {
  dispatch(getCategoriesBegin());

  return (fetch(BASE_URL))
    .then(res => res.json())
    .then((json) => {
      (dispatch(getCategoriesSuccess(json)));
    })
    .catch(err => dispatch(getCategoriesFailure(err)));
};

export const getItemsForCategory = () => ({
  type: FETCHING_CATEGORY_ITEMS_BEGIN,
});

export const getItemsForCategorySuccess = (category, data) => ({
  type: FETCHING_CATEGORY_ITEMS_SUCCESS,
  category,
  data,
});

export const getItemsForCategoryFailure = () => ({
  type: FETCHING_CATEGORY_ITEMS_FAILURE,
});

export const fetchItemsForCategory = category => (dispatch) => {
  dispatch(getItemsForCategory());
  return (fetch(`${BASE_URL}${category}`))
    .then(res => res.json())
    .then((json) => {
      (dispatch(getItemsForCategorySuccess(category, json)));
    })
    .catch(err => dispatch(getItemsForCategoryFailure(err)));
};
