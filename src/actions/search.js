// Types
export const SEARCH_QUERY_CHANGED = 'SEARCH_QUERY_CHANGED';
export const CLEAR_SEARCH_QUERY = 'CLEAR_SEARCH_QUERY';

// Action Creators
export const searchQueryChanged = text => ({
  type: SEARCH_QUERY_CHANGED,
  text,
});

export const clearSearchQuery = () => ({
  type: CLEAR_SEARCH_QUERY,
});
